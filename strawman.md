# Preamble

This is a proposal for creating and maintaining an M standard.

# General

Just as a software project is nothing more than a collection of source code, build scripts, automated tests, etc. that are under version control, standards are nothing more than a collection of files (including test programs) that are under version control. In both cases, all changes should be tracked, and every file should have an audit trail.

A trend in software is the move towards [Continuous Integration](https://en.wikipedia.org/wiki/Continuous_integration) and [Continuous Delivery](https://en.wikipedia.org/wiki/Continuous_delivery). Applying this to standards means that its documents continuously reflect the current state of the standard. In other words, there is no “1995” standard or “2021” standard. The standard documents are *always* current, and we never have to say “This proposal came in 10 seconds after the cutoff, and was too late to be considered.” Each change to the standard is accepted as a delta that changes one or more documents. It may take years for some proposed changes to be accepted, and minutes for others.

If we ever need to go to a standards organization that requires versioning or date stamping of standard, we can always designate a snapshot of the documents to be “Version 1.5” or “2020 Version.”

# Specific

This write-up proposes a standard maintained using [git](https://git-scm.com/) for distributed version control, and a public repository at [GitLab](https://gitlab.com). With minimal changes to the wording, the write-up could just as easily apply to a standard maintained at any of a number of other sites that have comparable functionality, such as GitHub or Bitbucket. Indeed, it is recommended that a GitHub project exists to mirror the GitLab project, just as projects at [https://github.com/YottaDB/](https://github.com/YottaDB/) mirror projects at [https://gitlab.com/YottaDB](https://gitlab.com/YottaDB).

Documents are in [GitLab Markdown](https://gitlab.com/gitlab-org/gitlab/blob/master/doc/user/markdown.md) which makes them directly readable. The can also be published ([https://gitlab.com/help/user/markdown](https://gitlab.com/help/user/markdown) is the same document published as a web page).

Changes to documents are tracked using Merge Requests. For example, https://gitlab.com/YottaDB/DB/YDB/-/merge_requests is the current set of Merge Requests for [YottaDB](https://gitlab.com/YottaDB/DB/YDB).

The motivation/rationale for Merge Requests (changes to functionality, requests for clarification, etc.) is managed as Issues. For example [https://gitlab.com/YottaDB/DBMS/YDBOcto/-/issues](https://gitlab.com/YottaDB/DBMS/YDBOcto/-/issues) is the current list of Issues for [Octo](https://gitlab.com/YottaDB/DBMS/YDBOcto/-/issues).

The project is a [publicly visible](https://docs.gitlab.com/ee/public_access/public_access.html) project. GitLab gives [free access to free / open source projects](https://about.gitlab.com/solutions/open-source/program/).

## Workflow

### Issues

Anyone with a GitLab id can create an Issue. The Issue describes at a functional or operational level a shortcoming of the current documentats and what the objective or motivation for change is. We have a set of Labels that can be used to categorize each Issue, e.g., Consistency; Language (grammar and typos); Enhancement, Won't Fix; etc. We may also have tags to identify issue origins, e.g., Ambuity in 1995 Standard, Approved by MDC after 1995, etc. An Issue can have multiple tags.

Anyone with a GitLab id can comment on an Issue, or give it a thumbs-up or -down.

An Issue can have one or more Merge Requests associated with it, and can be closed by a project member who has the requisite [permissions](https://gitlab.com/help/user/permissions) for the project, or the original creator. An Issue can be closed when it is withdrawn from consideration, a Merge Request implementing it is merged, it is subsumed by another Issue, etc.

### Merge Requests

Anyone who wishes to update a document must first [fork](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html) the repository to their personal GitLab repo, and presumably [clone](https://git-scm.com/docs/git-clone) that to their laptop or immediate working environment (strictly not required, but more immune to Internet hiccups). They would then create a [git branch](https://git-scm.com/docs/git-branch), update the branch, and make [signed commits](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work) and [git push](https://git-scm.com/docs/git-push) to their personal GitLab fork, from where they can submit a Merge Request. Each Merge Request should be tagged with the Issue(s) to which it responds.

Anyone can comment on Merge Requests. The submitter responds to comments on the Merge Request page (e.g., to rebut a comment), or by making, commiting, signing, and pushing additional changes.

Once all comments have been responded to, someone who has the permission to merge into the master repository can perform the actual merge. All merges must be [fast-forward merges](https://docs.gitlab.com/ee/user/project/merge_requests/fast_forward_merge.html).

## Workflow Example

The following is just to illustrate a possible flow through the system of one change. The starting point is a document in the standard that specifies `$horolog` with two pieces.

### Statement of Need

- Someone submits an Issue saying one-second resolution is insufficient. Perhaps that person proposes adding a decimal part to the second piece as a solution.
- In comments (discussion) on the Issue, someone else points out that time management also has a problem with timezones that adding a decimal part to seconds doesn't fix, and it would be good to solve the problem completely.
- Someone else proposes adding two more pieces to `$horolog`, the number of microseconds in the current second and the offset from UTC.
- Someone else brings up the issue of backward compatibility, and there is discussion back and forth as to whether anyone would be looking at the number of pieces of `$horolog`, etc.

### Change Proposal

- At some point, someone submits a Merge Request for specific wording changes to the document and test cases for the change. The Merge Request should identify the Issue(s) it purports to address.
- Someone else comments that there is a typo and the ISV is spelled `$heroblog` in one place. The Merge Request submitter corrects and pushes a change.
- Someone elses proposes adding to the Merge Request a `VIEW` keyword, initialized from an environment variable, controlling the format of `$horolog`. There is discussion back and forth as to whether this is useful or just bloating the language. The proposer of the `VIEW` command agrees to create a separate Issue for consideration.
- Seeing the dicussion die down, the Benevolent Dictator (or a committee of implementers, or other, as we may decide) asks for a vote on the final Merge Request.
- After a successful vote, the submitter may need to perform a [git rebase](https://git-scm.com/docs/git-rebase) since there may have been other changes merged since s/he last fetched the repository.
- Someone with Merge permissions merges the change. **The Merge makes the change to the standard official.**

## Clean Up

Either the original proposer, or someone with the appropriate permission notes the Merge Request number in the Issue(s) that it addresses, and closes the Issue.

# Audit and Control

- All Merge Requests must be signed with a public key.
- Anyone with update permissions to the repository must use two factor authentication to sign in to GitLab, with the second factor being an authentication app (e.g., Authy or Google Authenticator), nor a text message to a phone (to defend against SIM hijacking).

# Repository Organization

The top level of the repository has a directory for the actual standard and additional directories (e.g., for project governance rules). The actual standard is divided into directories for “Commands”, “Intrinsic Special Variables,” “Functions,” etc. This means that eventually the standard can become a reference manual for standard M, and individual implementations need only document how they differ from the standard, provide additional discussion and examples, etc.
